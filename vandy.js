var vandy = {};

vandy.settings = {
    class: "vandify",
    verbose: true,
    dataTimeout: 1, // TODO: test data for creation date
    formSettings: {
        saving: true,
        browserValidation: true,
        globalScope: false, // TODO: If true, dont care for form ID, just fill by name
        forceFormsaving: false
    },
    seoSettings: {

    }
};

vandy.init = function() {
    vandy.elements = {};
    vandy.elements.all = document.querySelectorAll("." + this.settings.class);
    vandy.elements.head = document.querySelector("head");
    if (vandy.settings.formSettings.saving) {
        var elementPool = vandy.elements.all;
        if (vandy.settings.formSettings.forceFormsaving) {
            elementPool = document.querySelectorAll("form");
        }
        vandy.elements.form = [];
        elementPool.forEach(function(e) {
            if (e.tagName.toLowerCase() === "form") {
                vandy.elements.form.push(e);
            }
        });
        vandy.elements.form.forEach(function(e) {
            vandy.forms.init(e);
        });
    }

    // TEMP TODO: Find better solution for this (data attrib? :))
    var formBuildBtn = document.querySelector("#rebuildForm");
    if (formBuildBtn !== null) {
        formBuildBtn.addEventListener('click', function(e) {
            var container = document.querySelector("#" + e.target.dataset.target);
            if (container !== undefined) {
                var data = vandy.forms.getStoredData('test');
                vandy.forms.createForm(data, container);
            }
        });
    }
    // TEMP
};

vandy.log = function(txt) {
    if (vandy.settings.verbose) {
        console.log(txt);
    }
};

vandy.forms = {
    init: function(form) {
        var fields = [].slice.call(form.querySelectorAll("input"));
        var data = this.getStoredData(form.id);
        fields = fields.concat([].slice.call(document.querySelectorAll("textarea[form=" + form.id + "]")));
        for (var i = 0; i < fields.length; i+=1) {
            var field = fields[i];
            if (field.type === "submit") {
                this.catchSubmit(field);
            } else {
                if (this.skipSaving.indexOf(field.name) === -1) {
                    if (data) {
                        this.testStoredData(field, data);
                    }
                    this.listenField(field);
                }
            }
        }
    },
    skipSaving: ["", "file", "button", "password", "reset", "image", "hidden", "search"],
    getFieldData: function(form) {
        var fields = [].slice.call(form.querySelectorAll("input"));
        var textAreas = document.querySelectorAll("textarea[form=" + form.id + "]");
        if (textAreas.length > 0) {
            fields = fields.concat([].slice.call());
        }
        var formData = {};
        formData.formId = form.id;
        formData.data = [];
        formData.url = window.location.origin + window.location.pathname; // TODO: Needs to be cleaner, strip #, variables etc
        formData.created = new Date().toLocaleString();
        for (var i = 0; i < fields.length; i+=1) {
            var field = fields[i];
            var blob = {
                name: field.name,
                value: field.value,
                type: field.type
            }
            if (this.skipSaving.indexOf(field.name) === -1) {
                if (field.type === "checkbox") {
                    blob.value = field.checked;
                    formData.data.push(blob);
                } else if (field.type === "radio") {
                    if (field.checked) {
                        blob.value = field.value;
                        formData.data.push(blob);
                    }
                } else {
                    formData.data.push(blob);
                }
            }
        }
        return formData;
    },
    getStoredData: function(formId, txt) {
        var data = localStorage.getItem("formData_" + formId);
        if (txt) {
            return data;
        } else {
            return JSON.parse(data);
        }
    },
    testStoredData: function(field, data) {
        data.data.forEach(function(d) {
            if (d.name === field.name) {
                if (field.type === "checkbox") {
                    field.checked = d.value;
                } else if (field.type === "radio" && field.name === d.name && field.value === d.value) {
                    field.checked = true;
                } else if (field.type !== "checkbox" && field.type !== "radio") {
                    field.value = d.value;
                }
            }
        });
    },
    listenField: function(field) {
        field.addEventListener("change", function() {
            vandy.forms.saveForm(field);
        });
    },
    saveForm: function(field) {
        if (vandy.settings.formSettings.saving) {
            var form = field.form;
            var formData = this.getFieldData(form);
            vandy.log("Saved:");
            vandy.log(formData);
            localStorage.setItem("formData_" + formData.formId, JSON.stringify(formData));
        }
    },
    catchSubmit: function(submit) {
        submit.addEventListener("click", function(e) {
            if (e.target.form.checkValidity()) {
                e.preventDefault();
                vandy.forms.saveForm(e.target);
                e.target.form.submit();
            }
        });
    },
    createForm: function(data, container) {
        container.innerHTML = null;
        if (data.formId === undefined) {
            data.formId = vandy.settings.class;
        }
        var form = vandy.UI.renderForm(container, data.formId); //container, name, action = "#", method = "post"
        vandy.form = form;
        var fields = data.data;
        for (var i = 0; i < fields.length; i++) {
            if (fields[i].type !== undefined && fields[i].name !== undefined) {
                var fieldContainer = vandy.UI.renderDiv('', form); //content, container, classes, id
                var field = vandy.UI.getField(fields[i].name, fields[i].value, fields[i].type);
                vandy.UI.renderText(vandy.tools.formatVar(fields[i].name), fieldContainer);
                fieldContainer.appendChild(field);
            }
        }
        var fieldContainer = vandy.UI.renderDiv('', form); //content, container, classes, id
        var field = vandy.UI.getField('submit', 'Save', 'submit');
        fieldContainer.appendChild(field);
        return form;
    },
    purge: function(formId) {
        localStorage.removeItem("formData_" + formId);
    }
};

vandy.seo = {
    init: function(GAID) {
        var tag = document.createElement("script");
        tag.innerHTML = this.code;
        vandy.elements.head.appendChild(tag);
        vandy.settings.seoSettings.GAID = GAID;
        ga('create', GAID, 'auto');
        ga('send', 'pageview');
    },
    code: "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');",
}

vandy.api = {
    settings: {
        server: '',
        base: '',
        withCredentials: false,
    },
    set: function(server, base, credentials) {
        this.settings.server = server;
        this.settings.base = base;
        this.settings.credentials = credentials;
    },
    call: function(data, res = function(u){console.log(u)}) {
        console.log(this.settings);
        var API = new XMLHttpRequest();
        API.open("POST", this.settings.server + this.settings.base);
        API.setRequestHeader("Content-Type", 'application/json');
        API.withCredentials = this.settings.withCredentials;
        API.onload = function() {
            if (API.status === 200) {
                return res(JSON.parse(API.responseText));
            } else {
                console.warn("API Call failed");
            }
        }
        // console.log(API);
        API.send(JSON.stringify(data));
    }
}

vandy.tools = {
    purgeLocalStorage: function() {
        localStorage.clear();
    },
    clean: function(string, selector, space, pop) {
        var strArray = string.toLowerCase().split(selector);
        var clean = "";
        if (pop) {
            strArray.pop();
        }
        for (var i = 0; i < strArray.length; i++) {
            clean += strArray[i];
            if (space) {
                clean += " ";
            }
        }
        return clean;
    },
    formatVar: function(str) {
        var arr = str.split('');
        var clean = '';
        for (var i = 0; i < arr.length; i++) {
            if (i === 0) {
                arr[i] = arr[i].toUpperCase();
            } else if (arr[i] === arr[i].toUpperCase()) {
                arr[i] = ' ' + arr[i].toLowerCase();
            }
            clean += arr[i];
        }
        return clean;
    },
    shuffle: function (array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    },
    site: {
        url: function() {

        },
        path: function() {
            
        }
    }
};

vandy.UI = {
    addClasses: function(element, classes) {
        if (classes) {
            if (typeof classes === 'string') {
                element.classList.add(classes);
                return element;
            } else if (classes.length > 0) {
                classes.forEach(function(cl) {
                    element.classList.add(cl);
                });
                return element;
            }
        }
        return element;
    },
    getText: function() {
        var el = document.createElement("p");
        return el;
    },
    renderText: function(string, container) {
        var el = this.getText();
        el.innerHTML = string;
        container.appendChild(el);
    },
    getDiv: function(classes, id) {
        var el = document.createElement("div");
        el = vandy.UI.addClasses(el, classes);
        if (id) {
            el.id = id;
        }
        return el;
    },
    renderDiv: function(content, container, classes, id) {
        var div = this.getDiv(classes, id);
        div.innerHTML = content;
        container.appendChild(div);
        return div;
    },
    getList: function(items) {
        var ul = document.createElement("ul");
        items.forEach(function(item) {
            var el = document.createElement("li");
            el.innerHTML = item;
            ul.appendChild(el);
        });
        return ul;
    },
    getForm: function(name, action, method) {
        var form = document.createElement("form");
        form.id = name;
        form.name = name;
        form.action = action;
        form.method = method;
        return form;
    },
    renderForm: function(container, name, action = "#", method = "post") {
        var form = this.getForm(name, action, method);
        container.appendChild(form);
        return form;
    },
    getField: function(name, value = null, type = 'text') {
        var input = document.createElement("input");
        input.name = name;
        input.type = type;
        if (type === 'checkbox' || type === 'radio') {
            input.checked = value;
        } else {
            input.value = value;
        }
        return input;
    }
}

vandy.fun = {
    cards: {
        // TODO: How do I wanna store stuff in RAM?
        storage: [],
        obj: {
            manager: function(options) {
                this.id = null;
                this.deck = new vandy.fun.cards.obj.deck(options);
                this.stacks = [];
            },
            deck: function(options) {
                var decks, joker;
                if (options.amountOfDecks) {
                    decks = options.amountOfDecks;
                } else {
                    decks = 1;
                }
                if (options.jokersPerDeck) {
                    joker = options.jokersPerDeck;
                } else {
                    joker = 2;
                }
                this.cards = [];
                for (var d = 0; d < decks; d++) {
                    for (var t = 0; t < 4; t++) {
                        // Loop from 1 (ace) till 13 (queen)
                        for (var v = 1; v < 14; v++) {
                            this.cards.push(new vandy.fun.cards.obj.card(t, v));
                        }
                    }
                }
                for (var j = 0; j < joker; j++) {
                    this.cards.push(new vandy.fun.cards.obj.card(-1, 0))
                }
                this.cards = vandy.tools.shuffle(this.cards);
            },
            card: function(type = 0, value = 0) {
                this.type = type;
                this.typeName = vandy.fun.cards.types(type);
                this.value = value;
            },
            stack: function(player = false, cards = []) {
                this.player = player;
                this.cards = cards;
            },
        },
        create: function(options = {}) {
            var newManager = new this.obj.manager(options);
            newManager.id = vandy.fun.cards.storage.length;
            vandy.fun.cards.storage.push(newManager);
            return vandy.fun.cards.storage[newManager.id];
        },
        addStack: function(deckID, isPlayer = false) {
            var stack = new vandy.fun.cards.obj.stack(isPlayer);
            stack.id = vandy.fun.cards.storage[deckID].stacks.length;
            vandy.fun.cards.storage[deckID].stacks.push(stack);
            return vandy.fun.cards.storage[deckID].stacks;
        },
        types: function(index) {
            switch(index) {
                case -1:
                    return 'joker';
                case 0:
                    return 'spades';
                case 1:
                    return 'hearts';
                case 2:
                    return 'diamonds';
                case 3:
                    return 'clubs';
            }
        },
        draw: function(deckID, toStackID) {
            var man = vandy.fun.cards.storage[deckID]
            var deck = man.deck;
            var stack = man.stacks[toStackID];
            if (!deck || !stack) {
                console.warn("Storage error");
                return null;
            }
            var card = deck.cards[0];
            stack.cards.push(card);
            deck.cards.splice(0,1);
            return card;
        }
    }
}

document.addEventListener("DOMContentLoaded", function() {
    vandy.init();
});

if (typeof module !== 'undefined') {
    module.exports = vandy;
}